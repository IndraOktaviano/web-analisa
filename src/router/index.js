import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Topik from '../views/Topik.vue'
import DetailTopik from '../views/Detail-Topik.vue'
import Isu from '../views/Isu.vue'
import Testing from '../views/testing.vue'
import DetailIsu from '../views/Detail-Isu.vue'
import PemberitaanTokoh from '../views/detail-page/PemberitaanTokoh.vue'
import StatementTokoh from '../views/detail-page/StatementTokoh.vue'
import SentimentTokoh from '../views/detail-page/SentimentTokoh.vue'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/topik',
    name: 'Topik',
    component: Topik
  },
  {
    path: '/detail-topik/:topik',
    name: 'Detail-Topik',
    component: DetailTopik,
    props: true,
  },
  {
    path: '/isu',
    name: 'Isu',
    component: Isu
  },
  {
    path: '/isu/detail-isu',
    name: 'Detail-Isu',
    component: DetailIsu
  },
  {
    path: '/detail-page/pemberitaantokoh',
    name: 'PemberitaanTokoh',
    component: PemberitaanTokoh
  },
  {
    path: '/detail-page/statementtokoh',
    name: 'StatementTokoh',
    component: StatementTokoh
  },
  {
    path: '/detail-page/sentimenttokoh',
    name: 'SentimentTokoh',
    component: SentimentTokoh
  },
  {
    path: '/testing',
    name: 'testing',
    component: Testing
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
