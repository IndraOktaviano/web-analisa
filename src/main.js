import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'font-awesome/css/font-awesome.css'
import './registerServiceWorker'
import VueApexCharts from 'vue3-apexcharts'
import axios from 'axios'
import cors from 'cors'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(VueApexCharts)
app.use(cors)

app.mixin({
    created() {
        const token = "Token aedb45c5cbb932a89febc37729e6839390460aee"
        if (token) {
            axios.defaults.baseURL = "https://dev.analytic.sanjari.dev/api/";
            axios.defaults.headers.common['Authorization'] = token            
        }
    }
})
app.mount('#app')