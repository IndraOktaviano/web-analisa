import axios from "axios";

export default {
    state: {
        daftarIsu: [],
        daftarTema: [],
        daftarNarasumber: [],
        totalPage: null,

        detailIsu: [],
    },
    mutations: {
        SET_DAFTAR_ISU(state, data) {
            state.daftarIsu = data;
        },
        SET_DAFTAR_TEMA(state, data) {
            state.daftarTema = data;
        },
        SET_DAFTAR_NARASUMBER(state, data) {
            state.daftarNarasumber = data;
        },
        SET_PAGE_ISU(state, data) {
            state.totalPage = data;
        },
        SET_DETAIL_ISU(state, data) {
            state.detailIsu = data;
        },
    },
    actions: {
        async getDaftarIsu({ commit }, data) {
            let start = "2020-01-01"
            let end = "2050-12-31"
            let page = "1"
            let limit = "10"
            let tema = ","
            let narsum = ","

            if (data) {
                start = data.start ? data.start : start
                end = data.end ? data.end : end
                page = data.page ? data.page : page
                limit = data.limit ? data.limit : limit
                tema = data.tema ? "," + data.tema : tema
                narsum = data.narsum ? "," + data.narsum : narsum
            }
            axios
                .get(
                    "source/21/data/?orient=records&page=" + page + "&limit=" + limit + "&field=orientasi_berita,tokoh_berita,tema_berita,judul_berita,nama_media_online,is_politik_name,link_berita,narasumber_1,narasumber_2,narasumber_3,jabatan_narasumber_1,jabatan_narasumber_2,jabatan_narasumber_3,acurate_berita,arah_pemberitaan,timestamp,waktu_terbitnya_berita&field_search=orientasi_berita,tema_berita,narasumber_1&values=Orientasi%20Isu" + tema + narsum + "&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end,
                )
                .then((response) => {
                    commit("SET_DAFTAR_ISU", JSON.parse(response.data.data.source.data_source));
                    commit("SET_PAGE_ISU", response.data.data.source.page_total);
                });
        },
        async getDaftarTema({ commit }) {
            axios
                .get(
                    "source/21/data-counts/?orient=records&field=tema_berita&field_search=orientasi_berita&values=Orientasi%20Isu",
                )
                .then((response) => {
                    commit("SET_DAFTAR_TEMA", JSON.parse(response.data.data.source.index));
                });
        },
        async getDaftarNarasumber({ commit }, tema) {
            axios
                .get(
                    "source/21/data/?orient=records&field=narasumber_1,narasumber_2,narasumber_3&field_search=tema_berita,orientasi_berita&values=" + tema + ",Orientasi%20Isu",
                )
                .then((response) => {
                    commit("SET_DAFTAR_NARASUMBER", JSON.parse(response.data.data.source.data_source));
                });
        },
    },
};
