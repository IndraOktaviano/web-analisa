import {
  createStore
} from 'vuex'

import tokoh from './tokoh'
import isu from './isu'
import dashboard from './dashboard'

import axios from 'axios'

export default createStore({
  state: {
    narasumber: [],
    date: { start: "2020-01-01", end: "2050-12-31" },
    listMediaFile: [],
    mediaFile: "21",
    chart1: "narasumber_1",
    chart2: "nama_media_online",
    chart3: "tokoh_berita",
    listChart: [],
  },
  getters: {
    findNarasumber: (state) => (narasumber_1) => {
      // ini getters nya
      console.log(narasumber_1)
      return state.daftarIsu.filter(daftarIsu => daftarIsu.tema_berita === narasumber_1)
    },
  },
  mutations: {
    SET_POSTS(state, narasumber) {
      state.narasumber = narasumber
    },
    SET_DATE(state, data) {
      state.date = data
    },
    SET_MEDIA_FILE(state, data) {
      state.mediaFile = data
    },
    SET_LIST_MEDIA_FILE(state, data) {
      state.listMediaFile = data
    },
    SET_CHART_1(state, data) {
      state.chart1 = data
    },
    SET_CHART_2(state, data) {
      state.chart2 = data
    },
    SET_CHART_3(state, data) {
      state.chart3 = data
    },
    SET_LIST_CHART(state, data) {
      state.listChart = data
    },
    UNSET_DATE(state) {
      state.date = { start: "2020-01-01", end: "2050-12-31" }
    }
  },
  actions: {
    getPosts({ commit }) {
      axios.get('https://dev.analytic.sanjari.dev/api/source/21/data/?orient=records&field=orientasi_berita,tema_berita,judul_berita,waktu_terbitnya_berita,nama_media_online,is_politik_name,link_berita,narasumber_1,narasumber_2,narasumber_3,jabatan_narasumber_1,jabatan_narasumber_2,jabatan_narasumber_3,acurate_berita,arah_pemberitaan,timestamp&field_search=orientasi_berita&values=Orientasi%20Isu&field_date=timestamp')
        .then(response => {
          commit('SET_POSTS', JSON.parse(response.data.data));
        })
    },
    getListMediaFile({ commit }) {
      axios.get('https://dev.analytic.sanjari.dev/api/sources/').then((res) => {
        // console.log(res);
        commit("SET_LIST_MEDIA_FILE", res.data)
      })
    },
    getListChart({ commit, state }) {
      console.log('asasas');
      axios.get("https://dev.analytic.sanjari.dev/api/source/" + state.mediaFile + "/columns/?orient=records").then((res) => {
        let dummy = []
        const vart = res.data.data.name
        vart.map(item => {
          if (!item.includes("Unnamed")) {
            dummy.push(item)
          }
        })
        commit("SET_LIST_CHART", dummy)
      })
    }
  },
  modules: {
    tokoh, isu, dashboard
  }
})