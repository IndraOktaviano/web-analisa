import axios from "axios";

export default {
    state: {
        dashboardStatement: [],
        dashboardSentiment: [],
        dashboardPemberitaan: [],
    },
    mutations: {
        SET_DASHBOARD_STATEMENT(state, data) {
            state.dashboardStatement = data;
        },
        SET_DASHBOARD_SENTIMENT(state, data) {
            state.dashboardSentiment = data;
        },
        SET_DASHBOARD_PEMBERITAAN(state, data) {
            state.dashboardPemberitaan = data;
        },
    },
    actions: {
        async getDasboardStatement({ commit, rootState }, data) {
            let start = rootState.date.start
            let end = rootState.date.end
            let limit = ""

            if (data) {
                limit = data.limit ?? limit
            }
            await axios
                .get(
                    "source/" + rootState.mediaFile + "/data-counts/?orient=records&limit=" + limit + "&field=" + rootState.chart1 + "&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end
                )
                .then((response) => {
                    let data = []
                    const counts = JSON.parse(response.data.data.source.counts)
                    const index = JSON.parse(response.data.data.source.index)

                    for (let j = 0; j < counts.length; j++) {
                        data.push(
                            {
                                name: index[j][0],
                                data: counts[j],
                            }
                        )
                    }
                    commit("SET_DASHBOARD_STATEMENT", data);

                });
        },
        async getDasboardSentiment({ commit, rootState }, data) {
            let start = rootState.date.start
            let end = rootState.date.end
            let limit = ""

            if (data) {
                limit = data.limit ?? limit
            }
            await axios
                .get(
                    "source/" + rootState.mediaFile + "/pivot/?orient=records&limit=" + limit + "&horizontal=arah_pemberitaan&vertical=" + rootState.chart3 + ",tema_berita&field_date=waktu_terbitnya_berita&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end
                )
                .then((response) => {
                    let data = {
                        total: [],
                        data: [],
                    }
                    const index = response.data.data.source.index
                    const pivot = JSON.parse(response.data.data.source.pivot)

                    for (let j = 0; j < index.length; j++) {
                        data.total.push({
                            positif: pivot[j].Positif ?? 0,
                            negatif: pivot[j].Negatif ?? 0,
                            netral: pivot[j].Netral ?? 0
                        })
                        data.data.push({ tokoh: index[j][0] })
                    }
                    commit("SET_DASHBOARD_SENTIMENT", data);

                });
        },
        async getDasboardPemberitaan({ commit, rootState }, data) {
            let start = rootState.date.start
            let end = rootState.date.end
            let limit = ""

            if (data) {
                limit = data.limit ?? limit
            }
            await axios
                .get(
                    "source/" + rootState.mediaFile + "/data-counts/?orient=records&limit=" + limit + "&field=" + rootState.chart2 + "&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end
                )
                .then((response) => {
                    let data = []
                    const counts = JSON.parse(response.data.data.source.counts)
                    const index = JSON.parse(response.data.data.source.index)

                    for (let j = 0; j < counts.length; j++) {
                        data.push(
                            {
                                name: index[j][0],
                                data: counts[j],
                            }
                        )
                    }
                    commit("SET_DASHBOARD_PEMBERITAAN", data);

                });
        },
    },
};
