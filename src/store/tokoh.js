import axios from "axios";

export default {
    state: {
        judulTokoh: null,
        totalTokoh: null,
        // 
        daftarTokoh: [],
        totalPage: null,

        statementTokoh: [],
        sentimentTokoh: [],
    },
    mutations: {
        SET_DAFTAR_TOKOH(state, data) {
            state.daftarTokoh = data;
        },
        SET_PAGE_TOKOH(state, data) {
            state.totalPage = data;
        },
        SET_JUDUL_TOKOH(state, data) {
            state.judulTokoh = data
        },
        SET_TOTAL_TOKOH(state, data) {
            state.totalTokoh = data
        },
        SET_STATEMENT_TOKOH(state, data) {
            state.statementTokoh = data
        },
        SET_SENTIMENT_TOKOH(state, data) {
            state.sentimentTokoh = data
        },
    },
    getters: {
        statementTokoh: state => {
            const data = state.statementTokoh;
            return tema => data.filter(item => {
                return item.tema === tema
            });
        },
        sentimentTokoh: state => {
            const data = state.sentimentTokoh;
            return tema => data.filter(item => {
                return item.tema === tema
            });
        },
    },
    actions: {
        async getDaftarTokoh({ commit, rootState }, data) {
            // let start = "2020-01-01"
            // let end = "2050-12-31"
            let page = "1"
            let limit = "10"

            if (data) {
                page = data.page ? data.page : page
                limit = data.limit ? data.limit : limit
            }
            await axios
                .get(
                    `source/21/word-cloud/?orient=records&field=judul_berita&filename=wordcloud5.png&width=500&height=500&limit=${limit}&page=${page}&field_date=waktu_terbitnya_berita&start_date=${rootState.date.start}&end_date=${rootState.date.end}`
                )
                .then((response) => {
                    let dummy = []
                    Object.keys(response.data.data.counts).slice(0, 30).map((key) => {
                        dummy.push(
                            {
                                judul: key,
                                total: response.data.data.counts[key]
                            }
                        )
                    })
                    commit("SET_DAFTAR_TOKOH", dummy);
                    commit("SET_PAGE_TOKOH", 1);
                });
        },
        async getStatmentTokoh({ commit, state }, data) {
            let start = "2020-01-01"
            let end = "2050-12-31"

            if (data) {
                start = data.start ? data.start : start
                end = data.end ? data.end : end
            }
            await axios
                .get(
                    "source/21/data-counts/?orient=records&field_search=judul_berita&values=" + state.judulTokoh + "&field=waktu_terbitnya_berita,tema_berita&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end
                )
                .then((response) => {
                    let data = []
                    const counts = JSON.parse(response.data.data.source.counts);
                    const index = JSON.parse(response.data.data.source.index);
                    for (let j = 0; j < counts.length; j++) {
                        data.push(
                            {
                                tanggal: index[j][0],
                                tema: index[j][1],
                                counts: counts[j]
                            }
                        )
                    }
                    commit("SET_STATEMENT_TOKOH", data)
                });
        },
        async getSentimentTokoh({ commit, state }, data) {
            let start = "2020-01-01"
            let end = "2050-12-31"

            if (data) {
                start = data.start ? data.start : start
                end = data.end ? data.end : end
            }
            axios
                .get(
                    "source/21/pivot/?orient=records&field_search=judul_berita&values=" + state.judulTokoh + "&horizontal=arah_pemberitaan&vertical=waktu_terbitnya_berita,tema_berita&field_date=waktu_terbitnya_berita&start_date=" + start + "&end_date=" + end
                )
                .then((response) => {
                    let data = []
                    const pivot = JSON.parse(response.data.data.source.pivot);
                    const index = response.data.data.source.index;
                    for (let j = 0; j < pivot.length; j++) {
                        data.push(
                            {
                                tanggal: index[j][0],
                                tema: index[j][1],
                                positif: pivot[j].Positif ? pivot[j].Positif : 0,
                                negatif: pivot[j].Negatif ? pivot[j].Negatif : 0,
                                netral: pivot[j].Netral ? pivot[j].Netral : 0,
                            }
                        )
                    }
                    commit("SET_SENTIMENT_TOKOH", data)
                });
        },
        async getCustomTokoh({ commit, rootState }, data) {
            console.log(data);
            return new Promise((resolve, reject) => {
                axios
                    .get(
                        `https://dev.analytic.sanjari.dev/api/source/21/pivot/?orient=records&field=judul_berita&field_search=judul_berita&values=${data.name}&horizontal=waktu_terbitnya_berita&vertical=waktu_terbitnya_berita,judul_berita&field_date=waktu_terbitnya_berita&start_date=${rootState.date.start}&end_date=${rootState.date.end}`
                    )
                    .then((response) => {
                        const cek = response.data.data.source.total_data
                        if (cek) {
                            commit("SET_JUDUL_TOKOH", data.name);
                            resolve();
                        }
                        else {
                            reject();
                        }
                        // commit("SET_JUDUL_TOKOH", )
                    });
            })
        },
    },
};
